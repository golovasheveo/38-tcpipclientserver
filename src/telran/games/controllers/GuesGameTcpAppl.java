package telran.games.controllers;

import telran.GuessGameTcpServer.GuessGameProtocol;
import telran.games.impl.BullsCowsGameImpl;
import telran.games.interfaces.GuessGame;
import telran.net.server.ProtocolJava;
import telran.net.server.ServerJava;

public class GuesGameTcpAppl {
    private static final int PORT = 4000;

    public static void main(String[] arg) {
        GuessGame game = new BullsCowsGameImpl();
        ProtocolJava protocol = new GuessGameProtocol(game);
        ServerJava server = new ServerJava(protocol, PORT);
        server.run();
    }
}
