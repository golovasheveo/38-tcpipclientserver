package telran.games.impl;

import telran.games.interfaces.GuessGame;
import telran.net.TcpClientJava;

import static telran.games.api.GuessGameApi.*;

public class GameTcpProxy extends TcpClientJava
        implements GuessGame {

    public GameTcpProxy(String host, int port) {
        super(host, port);
    }

    @Override
    public String startGame() {
        return sendRequest(START, null);
    }

    @Override
    public String prompt() {

        return sendRequest(PROMT, null);
    }


    @Override
    public String move(String userInput) {

        return sendRequest(MOVE, userInput);
    }

    @Override
    public boolean isFinished() {
        return sendRequest(IS_FINISHED, null);
    }

}
