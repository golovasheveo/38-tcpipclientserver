package telran.games.api;

public interface GuessGameApi {
    String START = "start";
    String PROMT = "promt";
    String MOVE = "move";
    String IS_FINISHED = "is finished";
}
